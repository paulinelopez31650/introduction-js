// Exercice 1 : 
// Créer une variable et l'initialiser à 0. 
// Tant que cette variable n'atteint pas 10 : 
// l'afficher, incrémenter de 1

 export const exo1 = ()=>{
    console.log('LES BOUCLES');
    console.log('Exercice1');
    for (let unNombre = 0; unNombre < 10; unNombre++) {
        console.log('voici mon nombre: ' + unNombre);
    }
    console.log('-----------------------------------------');
}

//Exercice 2:
//Créer deux variables. Initialiser la première à 0 et la deuxième avec un nombre compris en 1 et 100. 
// Tant que la première variable n'est pas supérieur à 20 :
// - multiplier la première variable avec la deuxième
// - afficher le résultat
// - incrémenter la première variable

let premierNombre=0;
let deuxiemeNombre= 30;

export const exo2 = ()=>{
    console.log('Exercice2');
    for (let premierNombre = 0; premierNombre < 20; premierNombre++) {
        let c= premierNombre*deuxiemeNombre;
        console.log(c);
    }
    console.log('-----------------------------------------');
}

// Exercice 3 :
// Créer deux variables. 
// Initialiser la première à 100 et la deuxième avec un nombre compris en 1 et 100. 
// Tant que la première variable n'est pas inférieur ou égale à 10 :
//     - multiplier la première variable avec la deuxième
//     - afficher le résultat
//     - décrémenter la première variable

let firstNombre= 100;
let secondNombre= 30;

export const exo3 = ()=>{
    console.log('Exercice3');
    while (firstNombre>=10)
    {  
        let c= firstNombre*secondNombre;
        console.log(firstNombre + "x" + secondNombre +"=" + c);
        firstNombre --;
    }
    console.log('-----------------------------------------');
}

// Exercice 4 :
// Créer une variable et l'initialiser à 1. Tant que cette variable n'atteint pas 10 :
//     - l'afficher
//     - l'incrementer de la moitié de sa valeur

let uneVariable=1;
export const exo4 = ()=>{
    console.log('Exercice4');
        while(uneVariable<=10){
            console.log(uneVariable);
            uneVariable+=uneVariable/2;
        }
    console.log('-----------------------------------------');
}

//Exercice 5:
//En allant de 1 à 15 avec un pas de 1, afficher le message On y arrive presque...


export const exo5 = ()=>{
    console.log('Exercice 5');

    for (let i = 1; i <= 15; i++) {
            console.log(i +" On y arrive presque");
        }
    console.log('-----------------------------------------');
}

//Exercice 6:
//En allant de 20 à 0 avec un pas de 1, afficher le message "C'est presque bon...".


export const exo6 = ()=>{
    console.log('Exercice 6');

    let bisou=20;
    while(bisou >0){
        bisou --;
        console.log(bisou +" C'est presque bon ...");
    }
    console.log('-----------------------------------------');
}


//Exercice 7: 
//En allant de 1 à 100 avec un pas de 15, afficher le message "On tient le bon bout..."."

export const exo7 = ()=>{
    console.log('Exercice 7');

    let miaou=1;
    while(miaou <100){
        miaou +=15;
        console.log(miaou +" On tient le bon bout...");
    }
    console.log('-----------------------------------------');
}

//Exercice 8 :
//En allant de 200 à 0 avec un pas de 12, afficher le message "Enfin ! ! !".

export const exo8 = ()=>{
    console.log('Exercice 8');

    let nono=200;
    while(nono >0){
        nono -=12;
        console.log(nono +" Enfin ! ! !");
    }
    console.log('-----------------------------------------');
}