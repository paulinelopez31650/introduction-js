// // Exercice 1 :
// // Créer un tableau "mois" et l'initialiser avec le nom des douze mois de l'année.

console.log('LES TABLEAUX');
console.log('Exercice 1');
const mois=[
        "Janvier",
        "Fevrier",
        "Mars",
        "Avril",
        "Mai",
        "Juin",
        "Juillet",
        "Aout",
        "Septembre",
        "Octobre",
        "Novembre",
        "Decembre",
    ];


// Exercice 2 :
// Avec le tableau de l'exercice 1, afficher la valeur de la troisième ligne de ce tableau.

export const exo2 = (mois)=>{
    console.log('Exercice 2');
    console.log(mois[2]);
}

// Exercice 3 :
// Avec le tableau de l'exercice , afficher la valeur de l'index 5.

$mois[5];


// Exercice 4 :
// Avec le tableau de l'exercice 1, modifier le mois de aout pour lui ajouter l'accent manquant.

$mois[7]="Août";

// Exercice 5 :
// Créer un tableau associatif avec comme index le numéro des départements des Hauts de France et 
// en valeur leur nom.

const departements =[
    "Aisne",
    "Nord ",
    "Oise" ,
    "Pas-de-Calais" ,
    "Somme" ,
    ]
console.log(departements);
//Exercice 6 :
//Avec le tableau de l'exercice 5, afficher la valeur de l'index 59.

mois[59];

//Exercice 7 :
// Avec le tableau de l'exercice 5, ajouter la ligne correspondant au département de la ville de Reims.

departements[5]="Marne";

//Exercice 8 :
//Avec le tableau de l'exercice 1 et une boucle, afficher toutes les valeurs de ce tableau.


foreach(moiss in mois)
{
   console.log("le mois de "+ moiss);
}


//Exercice 9 :
//Avec le tableau de l'exercice 5, afficher toutes les valeurs de ce tableau.

foreach(departement in departements)
{
   console.log("le departement est " + departement);
}

// Exercice 10 :
// Avec le tableau de l'exercice 5, afficher toutes les valeurs de ce tableau
// ainsi que les clés associés. Cela pourra être, par exemple, de la forme : 
// "Le département" + nom_departement + "a le numéro" + num_departement