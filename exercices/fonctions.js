// Exercice 1 : Faire une fonction qui retourne true.


//Exercice 2 :
//Faire une fonction qui prend en paramètre une chaine de caractères 
//et qui retourne cette même chaine.

//Exercice 3 :
//Faire une fonction qui prend en paramètre deux chaines de caractères 
//et qui renvoit la concaténation de ces deux chaines.

export const exo3 = (valeur1, valeur2)=>{
    console.log('Exercice 3');
    console.log(valeur1+" "+valeur2);
    console.log('-----------------------------------------');
}

//Exercice 4 :
//Faire une fonction qui prend en paramètre deux nombres. La fonction doit retourner :
// - "Le premier nombre est plus grand" si le premier nombre est plus grand que le deuxième,
// - "Le premier nombre est plus petit" si le premier nombre est plus petit que le deuxième,
// - "Les deux nombres sont identiques" si les deux nombres sont égaux,

export const exo4 = (number1,number2)=>{
    console.log('Exercice 4');
    if(number1>number2)
    {
        console.log("Le premier nombre est plus grand");
    }
    if(number1<number2)
    {
        console.log("Le premier nombre est plus petit");
    }
    if(number1==number2){
        console.log("Les deux nombres sont identiques");
    }
    console.log('-----------------------------------------');
}

// Exercice 5 :
// Faire une fonction qui prend en paramètre un nombre 
// et une chaine de caractères et qui renvoit la concaténation de ces deux paramètres.


export const exo5 = (cestUneValeur,cestUneAutreValeur)=>{
    console.log('Exercice 5');
    console.log(cestUneAutreValeur+" "+ cestUneValeur);
    console.log('-----------------------------------------');
}

//Exercice 6 :
// Faire une fonction qui prend trois paramètres : nom, prenom et age. 
// Elle doit renvoyer une chaine de la forme : "Bonjour" + nom + prenom + ", tu as " + age + "ans".


export const exo6 = (nom, prenom, age)=>{

    console.log('Exercice 6');
    console.log("Bonjour " + nom + " " + prenom + ", tu as " +age +" ans");
    console.log('-----------------------------------------');
}


// Exercice 7 :
// Faire une fonction qui prend deux paramètres : age et genre. 
// Le paramètre genre peut prendre comme valeur Homme ou Femme. 
// La fonction doit renvoyer en fonction des paramètres (gérer tous les cas) :
// - Vous êtes un homme et vous êtes majeur
// - Vous êtes un homme et vous êtes mineur
// - Vous êtes une femme et vous êtes majeur
// - Vous êtes une femme et vous êtes mineur


export const exo7 = (genre, age2)=>{
    console.log('Exercice 7');

    if(genre=="homme" && age2>=18){
        console.log("Vous êtes un homme et vous êtes majeur")
    }
    if(genre=="homme" && age2<18){
        console.log("Vous êtes un homme et vous êtes mineur")
    }
    if(genre=="femme" && age2<18){
        console.log("Vous êtes une femme et vous êtes mineur")
    }
    if(genre=="femme" && age2>=18){
        console.log("Vous êtes une femme et vous êtes majeur")
    }

    console.log('-----------------------------------------');
}

// Exercice 8 :
// Faire une fonction qui prend en paramètre trois nombres et qui renvoit la somme de ces nombres. 
// Tous les paramètres doivent avoir une valeur par défaut.

export const exo8 = (num1=5,num2=4,num3=45)=>{
    console.log('Exercice 8');
    let somme=num1+num2+num3
    console.log(somme);

}