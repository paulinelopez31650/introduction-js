import * as exercice1 from './exercices/boucles.js';
import * as exercice2 from './exercices/fonctions.js';
// import * as exercice3 from './exercices/tableaux.js';
import * as cesar from './exercices/cesar.js';

exercice1.exo1();
exercice1.exo2();
exercice1.exo3();
exercice1.exo4();
exercice1.exo5();
exercice1.exo6();
exercice1.exo7();
exercice1.exo8();

exercice2.exo3('popo', 'nono');
exercice2.exo4(90, 10);
exercice2.exo5('nono', 2);
exercice2.exo6('pauline', 'lopez', 23);
exercice2.exo7("femme",13);
exercice2.exo8(2,4);


// exercice3.exo2();

cesar.cesar();